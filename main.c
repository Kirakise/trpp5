#include <stdio.h>
#include <stdlib.h>
#include <string.h>

double CelcToKelv(double a)
{
	return (a + 273.15);
}

double CelcToFar(double a)
{
	return (9 * a / 5 + 32);
}
double FarToCelc(double a)
{
	return (5 * (a - 32) / 9);
}
double FarToKelv(double a)
{
	return (5 * (a - 32) / 9 + 273.15);
}
double KelvToCelc(double a)
{
	return (a - 273.15);
}
double KelvToFar(double a)
{
	return (9 * (a - 273.15) / 5) + 32;
}

int main(int argc, char **argv)
{
	double arg;

	if (argc != 4)
	{
		printf("Error - num of args\n");
		return (-1);
	}
	arg = atof(argv[1]);
	if (!strcmp(argv[2], "Celcius"))
	{
		if (!strcmp(argv[3], "Farengeight"))
			printf("%lf", CelcToFar(arg));
		else if (!strcmp(argv[3], "Kelvin"))
			printf("%lf", CelcToKelv(arg));
		else
		 {
			 printf("Error - 3-d arg\n");
			 return (-1);
		 }
	}
	else if (!strcmp(argv[2], "Farengeight"))
	{
		if (!strcmp(argv[3], "Celcius"))
			printf("%lf", FarToCelc(arg));
		else if (!strcmp(argv[3], "Kelvin"))
			printf("%lf\n", FarToKelv(arg));
		else
		{
			 printf("Error - 3-d arg\n");
			 return (-1);
		}
	}
	else if (!strcmp(argv[2], "Kelvin"))
	{
		if (!strcmp(argv[3], "Celcius"))
			printf("%lf\n", KelvToCelc(arg));
		else if (!strcmp(argv[3], "Farengeight"))
			printf("%lf\n", KelvToFar(arg));
		else
		{
			 printf("Error - 3-d arg\n");
			 return (-1);
		}
	}
	else {
		printf("Error - 2nd arg\n");
		return (-1);
	}
	return (0);
}
