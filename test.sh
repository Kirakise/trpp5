#!/bin/sh
for s1 in "Celcius" "Kelvin" "Farengeight"
do
    for s2 in "Celcius" "Kelvin" "Farengeight"
    do
        if [ "$s1" != "$s2" ] 
        then
            file="${s1}To${s2}.txt"
            for ((i=1; i < 100; i++))
            do
            a=$(( $RANDOM % 300 ))
            echo "INPUT: $a" >> $file
            tmp=$(./a.out $a $s1 $s2)
            echo "OUTPUT: $tmp" >> $file
            done
        fi
    done
done

